FROM a12e/docker-qt:5.13-gcc_64
MAINTAINER Black Hat <bhat@encom.eu.org>

RUN sudo apt-get update && \
    sudo apt-get -y install apt-transport-https ca-certificates gnupg software-properties-common wget && \
    wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | sudo apt-key add - && \
    sudo apt-add-repository -y 'deb https://apt.kitware.com/ubuntu/ xenial main' && \
    sudo apt-get update && \
    sudo apt-get -y install cmake && \
    sudo add-apt-repository -y ppa:ubuntu-toolchain-r/test && \
    sudo apt-get update && \
    sudo apt-get -y install gcc-9 g++-9 && \
    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 60 --slave /usr/bin/g++ g++ /usr/bin/g++-9 && \
    sudo update-alternatives --config gcc && \
    sudo apt-get -y install libgstreamer-plugins-base1.0-0 && \
    sudo apt-get clean
